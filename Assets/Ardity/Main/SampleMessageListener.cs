﻿using UnityEngine;
using System.Collections;

/**
 * 当创建你的消息监听器时，你需要实现这两个方法:  
 *  - OnMessageArrived
 *  - OnConnectionEvent
 */
public class SampleMessageListener : MonoBehaviour
{
    public static SampleMessageListener instance;
    public bool isMessage=false;
    /// <summary>
    /// 当从串行设备接收到一行数据时调用
    /// </summary>
    /// <param name="msg"></param>
    void OnMessageArrived(string msg)
    {
        //Debug.Log("Message arrived: " + msg);
        if (msg=="Hello World")
        {
            isMessage = true;
            Debug.Log("成功！！");
        }
    }

    // Invoked when a connect/disconnect event occurs. The parameter 'success'
    //当连接/断开连接事件发生时调用。 参数“成功”  
    // will be 'true' upon connection, and 'false' upon disconnection or failure to connect.
    //连接时为“true”，断开连接或连接失败时为“false”。  
    void OnConnectionEvent(bool success)
    {
        if (success)
            //Debug.Log("Connection established");
            Debug.Log("连接建立");
        else
            //Debug.Log("Connection attempt failed or disconnection detected");
            Debug.Log("连接尝试失败请检查串口！!");
    }
}

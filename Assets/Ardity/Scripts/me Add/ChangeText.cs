﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeText : MonoBehaviour
{
    public static ChangeText instance;
    public Text messageText;

    public SampleMessageListener SampleMessageListener;
    private void Awake( )
    {
        messageText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (SampleMessageListener.isMessage)
        {
            messageText.text = "我收到了信号！！";
        }
    }
}

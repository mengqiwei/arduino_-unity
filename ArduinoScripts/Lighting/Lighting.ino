void setup() {
  pinMode(13, OUTPUT); //将13号引脚设置成输出模式
  Serial.begin(9600);//波特率设置

}

void loop() {
  if (Serial.read() == 'q') //Serial.read()读取串口并返回收到的参数
  {
    digitalWrite(13, HIGH); //将13号引脚上的LED灯就亮了，输出高电位
    delay(1000);
    digitalWrite(13, LOW); //将13号引脚上的LED灯就灭了，输出低电位
    delay(1000);
    Serial.println("Hello World");
  }
  delay(1);
}
